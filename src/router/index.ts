import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import PlayersCatalogPage from "@/views/pages/players-catalog/players-catalog-page.vue";
import NewPlayerPage from "@/views/pages/new-player/new-player-page.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "players-catalog",
    meta: { title: "Каталог Футболистов" },
    component: PlayersCatalogPage,
  },
  {
    path: "/new-player",
    name: "new-player",
    meta: { title: "Добавить нового игрока" },
    component: NewPlayerPage,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
