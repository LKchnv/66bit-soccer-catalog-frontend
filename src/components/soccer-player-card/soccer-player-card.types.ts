import { IPlayerDto } from "@/dtos/player.dto";

export interface ISoccerPlayerCardProps {
  player: IPlayerDto;
}
