import { IPlayerDto } from "@/dtos/player.dto";

export interface IEditSoccerPlayerFormProps {
  state: IPlayerDto;
}
