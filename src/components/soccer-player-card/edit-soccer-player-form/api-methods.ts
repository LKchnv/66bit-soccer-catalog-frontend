import { IPlayerDto } from "@/dtos/player.dto";
import $api from "@/http/api";

export interface JsonPatch {
  op: string;
  path: string;
  value?: string | number;
}

export function generatePatchRequest(
  original: IPlayerDto,
  edited: IPlayerDto
): JsonPatch[] {
  const patches: JsonPatch[] = [];

  for (const key in original) {
    if (Object.prototype.hasOwnProperty.call(original, key)) {
      if (
        original[key as keyof IPlayerDto] !== edited[key as keyof IPlayerDto]
      ) {
        patches.push({
          op: "replace",
          path: `/${key}`,
          value: edited[key as keyof IPlayerDto],
        });
      }
    }
  }

  return patches;
}

export const patchPlayer = (
  playerId: number,
  originalPlayer: IPlayerDto,
  editedPlayer: IPlayerDto
) => {
  return $api.patch(
    `/api/v1/player/${playerId}`,
    generatePatchRequest(originalPlayer, editedPlayer)
  );
};
