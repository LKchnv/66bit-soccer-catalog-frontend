import { TGoogleMaterialIcons } from "../google-material-icon/google-material-icon.types";

export interface ICardLabelProps {
  heading: string;
  value: string;
  iconName: TGoogleMaterialIcons;
}
