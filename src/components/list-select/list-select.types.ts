export type IListSelectProps = IListSelectPropsWithoutDefaultSelection;

export interface IListSelectPropsWithoutDefaultSelection {
  menuItems: IListSelectMenuItem[];
  selectedItemId: number;
  isDefaultSelectionAvaliable: false;
  title: string;
}

export type IListSelectMenuItemProps = IListSelectMenuItem & {
  isSelected: boolean;
};

export interface IListSelectMenuItem {
  id: number;
  title: string;
}
