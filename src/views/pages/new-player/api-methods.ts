import $api from "@/http/api";
import { IPlayerDto } from "@/dtos/player.dto";
import { ITeamDto } from "@/dtos/team.dto";

export const postPlayer = (player: IPlayerDto) => {
  return $api.post<number>(`api/v1/player`, player);
};

export const postTeam = (
  state: { teams: ITeamDto[] },
  team: { TeamTitle: string }
) => {
  return $api.post<number>(`api/v1/team`, team);
};
