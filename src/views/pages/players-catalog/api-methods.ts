import $api from "@/http/api";
import { IPlayerDto } from "@/dtos/player.dto";

const getPlayers = (
  state: { data: IPlayerDto[] },
  limit: number,
  offset: number
) => {
  $api
    .get<IPlayerDto[]>(`/api/v1/player?Limit=${limit}&Offset=${offset}`)
    .then((response) => {
      state.data = response.data;
    });

  return;
};

export default getPlayers;
