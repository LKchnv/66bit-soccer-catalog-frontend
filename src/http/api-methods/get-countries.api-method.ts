import $api from "@/http/api";
import { ICountryDto } from "@/dtos/country.dto";

const getCountries = (state: { countries: ICountryDto[] }) => {
  $api.get<ICountryDto[]>(`/api/v1/country`).then((response) => {
    state.countries = response.data;
  });

  return;
};

export default getCountries;
