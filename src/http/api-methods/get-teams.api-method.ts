import $api from "@/http/api";
import { ITeamDto } from "@/dtos/team.dto";

const getTeams = (state: { teams: ITeamDto[] }) => {
  $api.get<ITeamDto[]>(`/api/v1/team`).then((response) => {
    state.teams = response.data;
  });

  return;
};

export default getTeams;
