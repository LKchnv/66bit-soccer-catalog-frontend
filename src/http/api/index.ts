import axios from "axios";

const $api = axios.create({
  baseURL: "https://66bit-soccer.snedson.com",
});

export default $api;
