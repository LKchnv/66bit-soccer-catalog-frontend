export interface ICountryDto {
  id: number;
  countryName: string;
}
