export interface ITeamDto {
  id: number;
  teamTitle: string;
}
