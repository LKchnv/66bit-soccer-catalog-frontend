export interface IPlayerDto {
  id: number;
  firstname: string;
  surname: string;
  gender: string;
  dateOfBirth: string;
  teamId: number;
  teamTitle: string;
  countryId: number;
  countryTitle: string;
}
